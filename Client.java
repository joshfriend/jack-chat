/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.KeyListener;
import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    public String name;
    public String serverAddress;
    public int port;
    IClientGUI gui;
    public Socket clientSocket;
    // public DataOutputStream toServer;
    // public BufferedReader fromServer;
    // public BufferedReader consoleIn;

    protected ServerListener serverListener;

    public Client(String name, String serverAddress, int port, IClientGUI gui) throws Exception{
        this.name = name;
        this.serverAddress = serverAddress;
        this.port = port;
        this.clientSocket = new Socket(this.serverAddress, port);
        this.gui = gui;
        gui.addQuit(quit);
        gui.addSend(send);
        gui.addSend(enter);
        gui.addWindowListenerz(exit);
    }

    public boolean login(String name) {
        boolean success = true;
        this.name = name;

        // Send registration message to server
        Message m = new Message(MessageType.REGISTER, name, "login");
        sendMessage(m);

        // Get server reply
        m = getMessage();

        if(m.toString().contains("server: ") && m.toString().contains("Currently online:")) {
            String check = m.toString();
            check = check.substring(8, check.length());
            String split[] = check.split("Currently online:");
            ArrayList<String> splitList = new ArrayList<String>(Arrays.asList(split));
            splitList.remove(0);
            for(int i = 0; i < splitList.size(); i++) {
                 splitList.get(i).trim();
            }
            gui.updateUserList(splitList);
        }
        gui.appendOutput(m.toString());

        if(m.messageType == MessageType.ERROR)
        {
            success = false;
            return success;
        }

        return success;
    }

    public void sessionStarted()
    {
        Message list = new Message("$list");
        sendMessage(list);
    }

    public String getConsoleInput() {
        String s = "";
        try {
            //Input from user
            s = gui.getText();
        }
        catch (Exception e) {
            //System.err.println(e);
            gui.appendOutput(e.toString());
        }
        return s;
    }

    public Message getMessage() {
        Message m = null;
        try {
            ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
            m = (Message) in.readObject();
        }
        catch (IOException e) {
            //System.err.println("Error retrieving message from server '" +
                               //this.clientSocket.getInetAddress().getHostName() + "'");
            gui.appendOutput("Error retrieving message from server '" +
                               this.clientSocket.getInetAddress().getHostName() + "'");
            System.exit(-1);
        }
        catch (ClassNotFoundException e) {
            //System.err.println("Unable to decode message!");
        }

        return m;
    }

    private void sendMessage(Message m) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(this.clientSocket.getOutputStream());
            out.writeObject(m);
            out.flush();
        }
        catch (IOException e) {
            //System.err.println("Error sending message to server '" +
                               //this.clientSocket.getInetAddress().getHostName() + "'");
            gui.appendOutput("Error sending message to server '" +
                               this.clientSocket.getInetAddress().getHostName() + "'");
        }
    }

    public void close() {
        try {
            this.clientSocket.close();
        }
        catch (IOException e) {
            System.out.println("Unable to close socket properly!");
            System.exit(-1);
        }
    }

    public ActionListener quit = new ActionListener(){
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try {
                clientSocket.close();
            }
            catch(IOException ex){
                System.out.println("Unable to close socket properly!");
                System.exit(-1);
            }
        }
    };

    public ActionListener send = new ActionListener(){
        @Override
        public void actionPerformed(ActionEvent e) {
            String input = getConsoleInput();
            Message m = new Message(input);
            gui.appendOutput("me: " + input);
            m.sender = name;
            sendMessage(m);
        }
    };

    public KeyListener enter = new KeyListener(){
        @Override
        public void keyPressed(KeyEvent k) {
            if(k.getExtendedKeyCode() == KeyEvent.VK_ENTER){
                                String input = getConsoleInput();
                Message m = new Message(input);
                                gui.appendOutput("me: " + input);
                m.sender = name;
                sendMessage(m);
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void keyTyped(KeyEvent e) {
            // TODO Auto-generated method stub

        }
    };

    public WindowListener exit = new WindowAdapter(){
        @Override
        public void windowClosing(WindowEvent e) {
            try {
                clientSocket.close();
                System.exit(0);
            }
            catch(IOException ex) {
                System.out.println("Unable to close socket properly!");
                System.exit(-1);
            }
        }
    };
}