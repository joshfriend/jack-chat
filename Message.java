/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class Message implements Serializable {
    public String sender;
    public ArrayList<String> recipients;
    public String message;
    public MessageType messageType;

    // Used if message is a command
    public String command;
    public ArrayList<String> args;

    public Message(String sender, ArrayList<String> recipients, MessageType type, String message) {
        this.sender = sender;
        this.recipients = recipients;
        this.messageType = type;
        this.message = message;
    }

    public Message(String raw) {
        raw = raw.trim();
        if(raw.startsWith("$"))
            parseCommand(raw);
        else
            parseMessage(raw);
    }

    public Message(MessageType t, String name, String message) {
        // Used for sending command/error/registration messages to/from server
        this.messageType = t;

        // Name field is the "from" of the message
        this.sender = name;
        this.message = message;
    }

    private void parseCommand(String raw) {
        // Remove special marker
        raw = raw.substring(1);

        // Split at space char
        ArrayList<String> args = new ArrayList<String>(Arrays.asList(raw.split(" ")));

        // Lowercase all args
        for(int i=0; i < args.size(); i++) {
            args.set(i, args.get(i).toLowerCase());
        }

        // First token is command word
        this.command = args.remove(0);

        // Remaining command arguments
        this.args = args;

        // Set message type
        this.messageType = MessageType.COMMAND;
    }

    private void parseMessage(String raw) {
        // Parse message string using regex
        String re = "@(\\w+)";
        Matcher m = Pattern.compile(re).matcher(raw);
        ArrayList<String> names = new ArrayList<String>();
        while (m.find()) {
            names.add(m.group(1).toLowerCase());
        }

        // Remove names from message
        raw = raw.replaceAll(re, " ").trim();

        // Default recipient is all if none are given
        if(names.size() == 0)
            names.add("all");

        // Remaining names are recipients
        this.recipients = names;

        // Message is whats left of raw after removing names
        this.message = raw;

        // Messages constructed this way are always text since they come from
        // user input at console in formatted string
        this.messageType = MessageType.TEXT;
    }

    // public String toString() {
    //     String s = "";
    //     if(this.sender != null)
    //         s += "From: " + this.sender + "\n";
    //     else
    //         s += "From: UNKNOWN\n";

    //     if(this.recipients != null)
    //         s += "To: " + this.recipients + "\n";
    //     else
    //         s += "To: NONE\n";

    //     if(this.messageType != null)
    //         s += "Type: " + this.messageType + "\n";
    //     else
    //         s += "Type: UNKNOWN\n";

    //     if(this.message != null)
    //         s += "Text: " + this.message + "\n";

    //     return s;
    // }

    public String toString() {
        String s = "";
        //if(System.getProperty("os.name").equals("Windows"))
            s = this.sender + ": " + this.message;
        //else
            //s = colorize();
        return s;
    }

    private String colorize() {
        String s = "";
        if(this.messageType == MessageType.ERROR)
            s = TermColors.RED + "ERROR: " + this.message + TermColors.RESET;
        else if(this.messageType == MessageType.TEXT)
            s = TermColors.YELLOW + this.sender + ": " + TermColors.CYAN + this.message + TermColors.RESET;
        else if(this.messageType == MessageType.COMMAND)
            s = TermColors.GREEN + this.command + " " + TermColors.GREEN + this.args + TermColors.RESET;
        else if(this.messageType == MessageType.REGISTER)
            s = TermColors.YELLOW + this.message + TermColors.RESET;
        return s;
    }
}
