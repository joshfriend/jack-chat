/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import java.io.*;
import java.net.*;
import java.util.concurrent.*;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;

public class ClientHandler extends Thread {
    private Socket connectionSocket;
    private String clientName;

    // Declared static so that there is only one instance of it for all client handlers.
    private static ConcurrentHashMap<String, ClientHandler> connectedClients;
    private static ArrayList<String> bannedUsers = new ArrayList<String>();             //list of banned usernames


    public ClientHandler(Socket socket, ConcurrentHashMap<String, ClientHandler> clients) {
        connectionSocket = socket;

        // Add self to server's list of connected clients
        connectedClients = clients;
    }

    public String getClientName() {
        return clientName;
    }

    public Message getMessage() {
        // Deserialize message object over TCP socket
        Message m = null;
        try {
            ObjectInputStream in = new ObjectInputStream(connectionSocket.getInputStream());
            m = (Message) in.readObject();
        }
        catch (EOFException e) {
            connectedClients.remove(clientName);
            for(ClientHandler ch : connectedClients.values()) {
                m = new Message(MessageType.REGISTER, clientName, clientName + " signed off...");
                m.args = new ArrayList<String>(connectedClients.keySet());
                if(ch.clientName.equals(clientName) == false) {
                    // Don't echo messages
                    ch.sendMessage(m);
                }
            }
            m = new Message(MessageType.REGISTER, clientName, clientName + " signed off...");
           //m.args.addAll(connectedClients.keySet());   <---- Causing error, removal didn't change anything.
        }
        catch (SocketException e) {
            System.err.println("Socket has been closed...");
        }
        catch (IOException e) {
            System.err.println("Error retrieving message from '" + clientName + "'");
            System.err.println(e);
        }
        catch (ClassNotFoundException e) {
            System.err.println("Unable to decode message!");
        }

        return m;
    }

    public void sendMessage(Message m) {
        // Serialize message over TCP socket
        try {
            ObjectOutputStream out = new ObjectOutputStream(connectionSocket.getOutputStream());
            out.writeObject(m);
            out.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error sending message to client '" +
                               connectionSocket.getInetAddress().getHostName() + "'");
        }
    }

    private boolean login() {
        // Handles login process for new clients, checks if client has been blacklisted, etc...
        Message m = getMessage();
        Message r = null;
        if(m.messageType == MessageType.REGISTER) {
            clientName = m.sender.toLowerCase();
            for(ConcurrentHashMap.Entry<String, ClientHandler> c : connectedClients.entrySet()) {
                System.out.println(c.getKey());
            }
            if(connectedClients.containsKey(m.sender.toLowerCase()) == false) {
                //do not connect if user is banned
                if(bannedUsers.contains(m.sender)){
                    r = new Message("@" + m.sender + " The username \"" + m.sender + "\" has been banned. Login denied.");
                    r.sender = "server";
                    r.messageType = MessageType.ERROR;
                }
                else {
                    // VALID LOGIN
                    connectedClients.put(clientName, this);
                    String clientList = "";
                    for(ConcurrentHashMap.Entry<String, ClientHandler> c : connectedClients.entrySet()) {
                        clientList += c.getKey() + "\n";
                    }
                    r = new Message(MessageType.REGISTER, "server", "Logged in as " + clientName + "!\nCurrently online:\n" + clientList);
                    r.args = new ArrayList<String>(connectedClients.keySet());

                    // Notify other users of login activity
                    for(ClientHandler ch : connectedClients.values()) {
                        if(ch.clientName.equals(clientName) == false) {
                            // Don't echo messages
                            Message l = new Message(MessageType.REGISTER, "server", clientName + " signed on...");
                            l.args = new ArrayList<String>(connectedClients.keySet());
                            ch.sendMessage(l);
                        }
                    }
                    System.out.println(clientName + " signed on...");
                }
            }
            else {
                r = new Message(MessageType.ERROR, "server", "Screen name already taken!");
            }
        }
        else {
            r = new Message(MessageType.ERROR, "server", "Need to log in first!");
        }

        sendMessage(r);
        if(r.messageType == MessageType.ERROR) {
            close();
            return false;
        }

        return true;
    }

    public void updateClientList(String message)
    {
        // Notify all users of who is signed on
        Message l = new Message(MessageType.REGISTER, "server", message);
        l.args = new ArrayList<String>(connectedClients.keySet());
        for(ClientHandler user : connectedClients.values()) {
            user.sendMessage(l);
        }
    }

    public void run() {
        // Register user first
        if(login() == false)
            return;

        while(true) {
            try {
                // Received message
                Message m = getMessage();

                // Try to exit while() loop if Message is null
                if (m == null)
                    break;

                // Placeholder for reply messages
                Message r = null;

                // Parse text messages between users
                if(m.messageType == MessageType.TEXT) {
                    // Default recipient is all
                    if(m.recipients.get(0).equals("all")) {
                        for(ClientHandler ch : connectedClients.values()) {
                            if(ch.clientName.equals(m.sender) == false)
                                // Don't echo messages
                                ch.sendMessage(m);
                        }
                    }
                    // Sent to specific users
                    else {
                        for(String recipient : m.recipients) {
                            ClientHandler ch = connectedClients.get(recipient);
                            if(ch != null) {
                                ch.sendMessage(m);
                            }
                            else {
                                r = new Message(MessageType.ERROR, "server", "No such user: '" + recipient + "'");
                                sendMessage(r);
                            }
                        }
                    }
                    System.out.println(m);
                }

                // Signon/off messages
                else if(m.messageType == MessageType.REGISTER) {
                    System.err.println(m);
                    break;
                }

                // Command message recieved
                else if(m.messageType == MessageType.COMMAND) {
                    //KICK - kick the specified user from the connected clients list
                    if(m.command.equalsIgnoreCase("kick")) {
                        // Message args is list of users to kick
                        for(String cname : m.args) {
                            ClientHandler ch = connectedClients.get(cname);  //to be kicked
                            if(ch != null) {
                                // Notify kicked user
                                r = new Message("@" + cname + " Sorry, you have been kicked.");
                                r.sender = m.sender;
                                ch.sendMessage(r);

                                // Close connection and kick from server
                                ch.close();
                                connectedClients.remove(cname);
                                updateClientList(cname + " has been kicked");
                            }
                            else {
                                r = new Message(MessageType.ERROR, "server", "No such user: '" + cname + "'");
                                sendMessage(r);
                            }
                        }
                    }

                    //BAN (based on username) - kick specified user and ban user from logging back in
                    else if(m.command.equalsIgnoreCase("ban")) {
                        // Message args is usernames to ban
                        for(String cname : m.args) {
                            ClientHandler ch = connectedClients.get(cname);
                            if(ch != null) {
                                // Notify banned user
                                r = new Message("@" + cname + " Sorry, you have been banned");
                                r.sender = "server";
                                ch.sendMessage(r);

                                // Add to server blacklist
                                bannedUsers.add(cname);

                                // Close connection and kick from server
                                ch.close();
                                connectedClients.remove(cname);

                                // Notify commander that user has been banned
                                updateClientList(cname + " has been banned");
                            }
                            else {
                                r = new Message(MessageType.ERROR, "server", "No such user: '" + cname + "'");
                                sendMessage(r);
                            }
                        }
                    }

                    //LIST - display all connected users
                    else if(m.command.equalsIgnoreCase("list")) {
                        ArrayList<String> recipient = new ArrayList<String>();  //send message back
                        recipient.add(m.sender);                                //to the sender

                        String list="\n\rOnline Users:\n\r";                    //get set of online users
                        Set<String> names = connectedClients.keySet();

                        // Check if any users connected
                        if(names.size() > 0) {
                            for(String n : names) {
                                list += n + "\n";
                            }
                        }
                        else {
                            // Should never get here because a connected client has to request this
                            // so at least 1 user has to be online
                            list += "ERROR: NO ONLINE USERS";
                        }

                        //send the message
                        r = new Message(MessageType.REGISTER, "server", list);
                        r.args = new ArrayList<String>(connectedClients.keySet());
                        ClientHandler ch = connectedClients.get(m.sender);
                        if(ch != null)
                            ch.sendMessage(r);
                        else
                            // Happens when no clients are left
                            break;
                    }

                    //EXIT/LOGOUT - log user out of session
                    else if((m.command.equalsIgnoreCase("logout")) || (m.command.equalsIgnoreCase("exit"))) {
                        connectedClients.get(m.sender).close();
                        connectedClients.remove(m.sender);
                        updateClientList(clientName + " signed off...");
                    }

                    //COMMANDS - display list of commands
                    else if(m.command.equalsIgnoreCase("cmds") || m.command.equalsIgnoreCase("help")){
                        String str = "@" + m.sender + " \nAll commands are preceded by the \"$\" symbol\n";
                        str += "Some commands have arguments and others do not\n";
                        str += "Chat commands are:\n";
                        str += "$list\tDisplay all current users\n";
                        str += "$kick\tKick specified user(s)\n";
                        str += "$ban\tBan the spcified user(s)\n";
                        str += "$logout\tLog out of your current session\n";
                        str += "$exit\tLog out of your current session\n";
                        str += "$cmds\tDisplay all commands\n";
                        str += "$help\tDisplay all commands\n";
                        r = new Message(str);
                        r.sender = "server";
                        connectedClients.get(m.sender).sendMessage(r);
                    }

                    //DEFAULT - unrecognizable command
                    else{
                        r = new Message("@" + m.sender + " ERROR: \"" + m.command + "\" is an invalid command.");
                        r.sender = "server";
                        connectedClients.get(m.sender).sendMessage(r);
                        r = new Message("@" + m.sender + " Type $cmds to view all commands.");
                        r.sender = "server";
                        connectedClients.get(m.sender).sendMessage(r);
                    }

                }
                else {
                    System.out.println(m);
                }
            }
            catch(Exception e) {
                System.out.println("Got an exception: " + e);
                // e.printStackTrace();
                break;
            }
        }
    }

    public void close() {
        try {
            connectionSocket.close();
        }
        catch (IOException e) {
            System.err.println(e);
        }
    }
}
