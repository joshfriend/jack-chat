 /*CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class ClientRun {

    public static void main(String args[]) throws Exception {
        Object portIn = null;
        String portString = null;
        int port = -1;
        JFrame frame = new JFrame();
        Object nameIn = null;
        String name = null;
        String server = null;
        Object serverIn = null;

        try {
            // Get username
            nameIn = JOptionPane.showInputDialog(frame, "Enter your user name below. NOTE: do not use the space character: ");
            name = nameIn.toString();
            if(name.contains(" ")){
                while(name.contains(" ")){
                    nameIn = JOptionPane.showInputDialog(frame, "Enter your user name below. NOTE: do not use the space character: ");
                    name = nameIn.toString();
                }
            }
            if(name.equals("")) {
                // Username defaults to login name of computer
                name = System.getProperty("user.name");
            }

            // Make ABSOLUTELY sure that the usernames are all lowercase
            name = name.toLowerCase();

            // Get server address
            serverIn = JOptionPane.showInputDialog(frame, "Enter a server address: ");
            server = serverIn.toString();
            if(server.equals("")) {
                // Default server address is localhost
                server = "localhost";
            }

            // Get server port number
            portIn = JOptionPane.showInputDialog(frame, "Enter a server port: ");
            portString = portIn.toString();
            if(portString.equals("")) {
                // Default server port is 9876
                port = 9876;
            }
            else {
                while(port == -1) {
                    try {
                        port = Integer.parseInt(portString);
                    }
                    catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(frame, "Invalid port: '" + portString + "'!");
                        port = -1;
                    }
                    // Check port range
                    if(port < 1024) {
                        JOptionPane.showMessageDialog(frame, "Port too low (< 1024): " + port);
                        port = -1;
                    }
                    else if(port > Math.pow(2, 16)) {
                        JOptionPane.showMessageDialog(frame, "Port too high (> " + (int) Math.pow(2, 16) + "): " + port);
                        port = -1;
                    }
                    portIn = JOptionPane.showInputDialog(frame, "Enter a server port: ");
                    portString = portIn.toString();
                    if(portString.equals("")) {
                        // Default server port is 9876
                        port = 9876;
                    }
                }
            }
        }
        catch (NullPointerException npe) {
            JOptionPane.showMessageDialog(frame, "Cancelled application.");
            System.exit(0);
        }

        Client client = null;
        Boolean connected = false;
        IClientGUI gui = new ClientGUI(name);

        while(!connected)
        {
            try
            {
                client = new Client(name, server, port, gui);
                gui.setVisible();
                connected = true;
            }
            catch (Exception e) //Escalated catch from "client".
            {

                nameIn = JOptionPane.showInputDialog(frame,
                        "Connection to server " + server + " with user name "
                        + name + " was unsuccessful. \n Please re-enter name: "
                        + "\n Enter 'quit' to cancel");

                name = nameIn.toString();
                if(name.equals("quit"))
                {
                    System.exit(0);
                }

                serverIn = JOptionPane.showInputDialog(frame,
                        "Please re-enter server address: ");
                server = serverIn.toString();
            }

        }

        if(client.login(name))
        {
            client.serverListener = new ServerListener(client, gui);
            client.serverListener.start();
        }
    }
}