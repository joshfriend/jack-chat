/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import java.io.*;
import java.net.*;
import java.util.concurrent.*;

public class Server {
    private ServerSocket listenSocket;
    private ServerMenu menu;
    private ConcurrentHashMap<String, ClientHandler> connectedClients;

    public Server(int port) {
        try {
            listenSocket = new ServerSocket(port);
            connectedClients = new ConcurrentHashMap<String, ClientHandler>();
            System.out.println("Started server... ");
        }
        catch (IOException e) {
            System.out.println("Server could not start on " + port);
            System.exit(-1);
        }
    }

    public void listen() {
        // Listens forever (or until terminated)
        while(! listenSocket.isClosed()) {
            if (! listenSocket.isClosed()) {
            try {
                Socket s = listenSocket.accept();
                ClientHandler c = new ClientHandler(s, this.connectedClients);
                c.start();
            }
            // ServerSocket.accept() throws SocketException when closing the TCP Socket
            catch (SocketException e) {
                System.out.println("Server shutting down... ");
                System.exit(0);
            }
            catch (IOException e) {
                System.err.println("Error accepting new connection:");
                e.printStackTrace();
                System.exit(-1);
            }
            }
        }
    }

    public void close() {
        for (ClientHandler ch : connectedClients.values() ) {
            if(ch != null) {
                // Notify users
                Message r = new Message("@" + ch.getClientName() + " Sorry, disconnected by Server.");
                r.sender = "Server";
                ch.sendMessage(r);

                // Close connection and kick from server
                ch.close();
                connectedClients.remove(ch.getClientName());
            }
        }

        try {
            this.listenSocket.close();
            System.out.println("Server port " + this.listenSocket.getLocalPort() + " closed...");
        }
        catch (IOException e) {
            System.err.println("Unable to close server socket!");
        }
    }

    public String toString() {
        return "<Server (" + listenSocket.getInetAddress().getHostName() + ":" + listenSocket.getLocalPort() + ")>";
    }

    /* instance method for displaying (and maybe processing) server console commands.
        Perhaps the actual command processing should happen in main() ? */

    public void mainMenu() {
        System.out.print("By your command: in Server ");
    }

    public void listUsers() {
        /* iterate thru connected users list */
        for(ConcurrentHashMap.Entry<String, ClientHandler> c : connectedClients.entrySet()) {
            System.out.println(c.getKey());
        }
    }

    public static void main(String args[]) {
        int port = 9876;

        // Parse commandline arguments
        if(args.length > 1) {
            System.err.println("Usage:\n$ java Server <port>");
            System.exit(-1);
        }
        else if(args.length == 1) {
            // Validate port number
            try {
                port = Integer.parseInt(args[0]);
            }
            catch (Exception e) {
                System.err.println("Error parsing port: '" + args[0] + "'");
                System.exit(-1);
            }

            // Check port range
            if(port < 1024) {
                System.err.println("Port too low (< 1024): " + port);
                System.exit(-1);
            }
            else if(port > Math.pow(2, 16)) {
                System.err.println("Port too high (> " + (int) Math.pow(2, 16) + "): " + port);
                System.exit(-1);
            }
        }
        else {
            System.out.println("Using default port: " + port);
        }

        // Create instance of server
        Server server = new Server(port);
        System.out.println(server);

        // Begin text-based control console thread
        // server.mainMenu();

        ServerMenu menu = new ServerMenu(server);
        menu.start();

        // Tell server to begin listening
        server.listen();

        // Close server
        server.close();
    }
}
