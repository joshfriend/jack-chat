/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.JDialog;
import javax.swing.JOptionPane;


public class ClientGUI extends javax.swing.JFrame implements IClientGUI {

    /**
     * Creates new form ClientGUI
     */
    private Date date = new Date();
    private String strDateFormat = "h:mm a";
    private SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
    private String name;

    public ClientGUI(String name) {
        this.name = name;
        initComponents();
        this.setName("J.A.C.K. Chat Client");
        signOn.setText("Signed in as: " + name);

        setVisible(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        consoleOutText = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        msgTextArea = new javax.swing.JTextArea();
        sendButton = new javax.swing.JButton();
        quitButton = new javax.swing.JButton();
        signOn = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        userOnlineField = new javax.swing.JTextArea();
        userOnlineLabel = new javax.swing.JLabel();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(358, 0), new java.awt.Dimension(358, 0), new java.awt.Dimension(358, 32767));
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu15 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("J.A.C.K Chat Client");
        setBackground(new java.awt.Color(0, 102, 102));
        setForeground(java.awt.Color.black);

        consoleOutText.setEditable(false);
        consoleOutText.setColumns(20);
        consoleOutText.setLineWrap(true);
        consoleOutText.setRows(5);
        consoleOutText.setWrapStyleWord(true);
        consoleOutText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jScrollPane1.setViewportView(consoleOutText);

        msgTextArea.setColumns(5);
        msgTextArea.setLineWrap(true);
        msgTextArea.setRows(5);
        msgTextArea.setToolTipText("");
        msgTextArea.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        msgTextArea.setSelectionColor(new java.awt.Color(0, 0, 0));
        jScrollPane2.setViewportView(msgTextArea);

        sendButton.setText("Send");

        quitButton.setText("Quit");
        quitButton.setMaximumSize(new java.awt.Dimension(57, 23));
        quitButton.setMinimumSize(new java.awt.Dimension(57, 23));
        quitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitButtonActionPerformed(evt);
            }
        });

        signOn.setForeground(new java.awt.Color(58, 132, 175));
        signOn.setText("Signed on as: ");

        userOnlineField.setEditable(false);
        userOnlineField.setColumns(20);
        userOnlineField.setLineWrap(true);
        userOnlineField.setRows(5);
        userOnlineField.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        userOnlineField.setMaximumSize(new java.awt.Dimension(150, 94));
        jScrollPane3.setViewportView(userOnlineField);

        userOnlineLabel.setForeground(new java.awt.Color(58, 132, 175));
        userOnlineLabel.setText("Users Online");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/signature.png"))); // NOI18N

        jMenu15.setText("Menu");

        jMenuItem1.setText("Help");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu15.add(jMenuItem1);

        jMenuBar1.add(jMenu15);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sendButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(quitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(signOn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(userOnlineLabel)
                        .addGap(26, 26, 26))))
            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(signOn)
                    .addComponent(userOnlineLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane3)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(sendButton)
                        .addComponent(quitButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void quitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quitButtonActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        String information =
                "A simple chat client, brought to you by J.A.C.K \n"
                + "-------------------------------------------------------------- \n"
                + "Append @ symbol to username (or multiple usernames\n"
                + "separated by spaces) to send a message to other Users.\n"
                + "If no username specified then message sent to everyone.\n\n"
                + "Commands (Type $ then the command): \n\t ex: $kick ace. \n\n\t "
                + "1. kick: kick a user from the server \n\t 2. ban: ban a user"
                + " from the server. \n\t 3. list: Display all current users on"
                + "the server \n\t 4. logout: Log out of your current session."
                + "\n\t 5. exit: exit your current session. \n\t 6. help: display"
                + " all commands.";
        JOptionPane optionPane = new JOptionPane();
        optionPane.setMessage(information);
        optionPane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
        JDialog dialog = optionPane.createDialog(null, "How To Play");
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea consoleOutText;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu15;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea msgTextArea;
    private javax.swing.JButton quitButton;
    private javax.swing.JButton sendButton;
    private javax.swing.JLabel signOn;
    private javax.swing.JTextArea userOnlineField;
    private javax.swing.JLabel userOnlineLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void addSend(ActionListener a) {
       sendButton.addActionListener(a);
    }

    @Override
    public void addSend(KeyListener k) {
        sendButton.addKeyListener(k);
        msgTextArea.addKeyListener(k);
    }

    @Override
    public void addQuit(ActionListener a) {
        quitButton.addActionListener(a);
    }

    @Override
    public String getText() {
        String text = msgTextArea.getText();
        msgTextArea.setText("");
        return text;
    }

    @Override
    public void appendOutput(String message) {
        //I didn't need all of this date information, but I didn't want to
        //block the messgage if a person says their name as well...
        //More or less just a precaution.

        message = "(" + sdf.format(date) + ") \r" + message + "\n";

        if(!message.contains("(" + sdf.format(date) + ") \r" + name))
        {
            consoleOutText.append(message);
            consoleOutText.setCaretPosition(consoleOutText.getDocument().getLength());
        }
    }

    @Override
    public void setVisible() {
        setVisible(true);
    }

    @Override
    public void addWindowListenerz(WindowListener e) {
        addWindowListener(e);
    }

    @Override
    public void updateUserList(ArrayList<String> users) {
        userOnlineField.setText("");
        for(String name : users) {
            userOnlineField.append(name + "\n");
        }
    }


}
