/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import java.io.*;
import java.net.*;
import java.util.*;

/* Attempts to create a shell console for the terminal.
    Possible features:
    List users, Kick userID, Ban userID, Create chat room (group)

    */

class ServerMenu extends Thread {  // need this class even be threaded??

    private Server server;

    public ServerMenu (Server s) {
        this.server = s;
    }

    void printHelp() {
        System.out.println("Type 'l' to list users on the server.");
        System.out.println("Type 'q' to notify users and quit the server.");
    }

    public void run() {
        char c = '?';
        String cmd = "";

        do {
            System.out.println("Enter command at any time: ");
            try {
                //Input from user
                BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
                cmd = console.readLine();
            }
            catch (Exception e) {
                System.err.println(e);
            }
            c = cmd.toLowerCase().charAt(0);

            switch (c) {
                case 'h': printHelp();
                break;

                case 'l': server.listUsers();
                break;

                case 'q':
                server.close();
                break;

                default:
                System.out.println("Type 'h' for help.");
            }

        } while(c != 'q');
    }
}