# ------------------------------------------
#
#  CIS457 - Data Communications
#  Project 1 - TCP Chat Client/Server
#  GVSU 2013
#
#               **STARRING**
#     __     ______     ______     __  __
#    /\ \   /\  __ \   /\  ___\   /\ \/ /
#   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
#  /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
#  \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
#    Josh       Aaron      Chris      Kyle
#
# ------------------------------------------

JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
.java.class:
		$(JC) $(JFLAGS) $*.java

CLASSES = \
		Client.java \
		ClientHandler.java \
		Message.java \
		Server.java \
		ServerListener.java \
		ServerMenu.java \
		TermColors.java \
		MessageType.java \
		ClientGUI.java \
		ClientRun.java \
		IClientGUI.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
		$(RM) *.class
