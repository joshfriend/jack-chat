/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

------------------------------------------ */

import java.text.SimpleDateFormat;
import java.util.*;

class ServerListener extends Thread {
    private Client client;
        IClientGUI gui;

    public ServerListener(Client c, IClientGUI gui) {
        this.client = c;
        this.gui = gui;
    }
    public void run() {
        while(true)
        {
            try {
                Message m = client.getMessage();

                if(m.messageType == MessageType.REGISTER) {
                    // update users list
                    gui.updateUserList(m.args);
                }
                // Show message in GUI
                //System.out.print("\r" + m + "\n> ");

                gui.appendOutput(m.toString());

                // Add short delay so Message is displayed before Socket connection closed
                try {
                    Thread.sleep(1500);
                }
                catch (Exception e) {
                    System.err.println("Error sleeping thread in ServerListener.run():");
                    e.printStackTrace();
                    System.exit(-1);
                }

            }
            catch(Exception e) {
                //System.out.println("ERROR:" + e);
                gui.appendOutput("ERROR: " + e);
                break;
            }
        }
    }
}