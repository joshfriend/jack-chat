# TCP Chat Client/Server Application

Project 1<br>
CIS457<br>
GVSU, Winter 2013

## Authors

* Chris Carr
* Josh Friend
* Aaron Herndon
* Kyle Peltier

## FEATURES

* Message all connected clients
* Message specific client(s)
* Kick/Ban users from server interface or client interface

### CLIENT:

* Start program
* Choose username, server address, and port (defaults: computer username, localhost, 9876)
* Send message to all by typing message and hitting ENTER or "Send" button
* Send message to specific users with "@name" tag format
* Send commands to server with "$cmd <args>" tag format

### SERVER:

* Data Structure: (ConcurrentHashMap) of ClientHandler and corresponding userID signed in on that ClientHandler
* Error handling: sends userID already in use message to client
